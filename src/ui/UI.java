/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import agregaciones.Carrera;
import agregaciones.Estudiante;
import asociaciones.bl.Banco;
import asociaciones.bl.Empleado;


/**
 *
 * @author Luis
 */
public class UI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Empleado miEmpleado;
        miEmpleado = new Empleado ("Krause" , "1-17-54-04-82");
        Banco miBanco;
        miBanco = new Banco ("BCR", "A 100mts de la CCSS", miEmpleado);
        System.out.println("Relacion entre clases : Asociación");
        System.out.println(miBanco);
        
        Estudiante miEstudiante = new Estudiante ("Krause Kelsen", "1-1754-0482");
        
        Carrera miCarrera = new Carrera ("Ingenieria Software");
        miCarrera.getMiEstudiante().add(miEstudiante);
        System.out.println("Relacion entre clases : Agregación");
        System.out.println(miCarrera);
    }
    //Para resguardar por encima debo hacer un commit
    //Git status, commit, push
}
