
package asociaciones.bl;

import java.util.ArrayList;


public class Banco {
    private String nombre;
    private String direccion;
    private Empleado miEmpleado;

    public Banco() {
    }

    public Banco(String nombre, String direccion, Empleado miEmpleado) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.miEmpleado = miEmpleado;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Empleado getMiEmpleado() {
        return miEmpleado;
    }

    public void setMiEmpleado(Empleado miEmpleado) {
        this.miEmpleado = miEmpleado;
    }

    @Override
    public String toString() {
        return "Banco: " 
                + " Nombre: " 
                + nombre 
                + " Dirección: " 
                + direccion 
                + " Mis Empleados: " 
                + miEmpleado;
    }
    
    
}
