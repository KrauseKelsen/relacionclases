
package agregaciones;

import java.util.ArrayList;


public class Carrera {
    private String nombre;
    private ArrayList <Estudiante> miEstudiante;

    public Carrera() {
    }

    public Carrera(String nombre) {
        this.miEstudiante = new ArrayList();
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList <Estudiante> getMiEstudiante() {
        return miEstudiante;
    }

    public void setMiEstudiante(ArrayList <Estudiante> miEstudiante) {
        this.miEstudiante = miEstudiante;
    }
   
    @Override
    public String toString() {
        return "Carrera{" + "nombre=" + nombre + ", miEstudiante=" + miEstudiante.toString() + '}';
    }
    
    
}
